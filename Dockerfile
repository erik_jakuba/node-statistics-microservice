FROM node:10
ENV PORT 8091
EXPOSE 8091

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json .
RUN npm install
COPY . .
ENV NODE_ENV production

CMD ["npm", "run", "prod"]
