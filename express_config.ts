/* config file */ 
export const optionsBodyParser = {inflate: true, limit: '1000kb', type: '*/*'};
export const corsOptions = {origin: '*'};

module.exports = {optionsBodyParser, corsOptions};
export default {optionsBodyParser, corsOptions};