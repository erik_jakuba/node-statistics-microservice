'use strict';
import * as config from "./express_config";
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const cors = require('cors');
const bodyParser = require('body-parser');
const httpReq = require('http');
const cron = require('node-cron');
const mongoose = require('mongoose');
app.use(bodyParser.json());
app.use(bodyParser.raw(config.optionsBodyParser));
app.use(cors(config.corsOptions));
mongoose.connect('mongodb://admin-user:admin-password@mongo:27017/admin?retryWrites=true&w=majority');
const StreamConnection = mongoose.model('StreamConnection', { clientId: String, streamId: String, clientEmail: String, timestamp: String, timeLeft: String, active: Boolean });
let clients = [];
let count = 0;
io.set('origins', '*:*');
io.on('connection', socket => {
    console.log(socket.id);
    io.emit('skt_init', socket.id);
    const dt = new Date();
    const utcDate = dt.toUTCString();
    clients.push({ clientId: socket.id, streamId: '', clientEmail: '', timestamp: utcDate, timeLeft: '', active: true });
    count = count + 1;
    console.log('CONNECTION STARTED');
    console.log(count);
    socket.on('disconnect', (data) => {
        console.log('Preparing for disconnecting the client...');
        for (var i = 0, len = clients.length; i < len; ++i) {
            var c = clients[i];
            const dtLeft = new Date();
            const utcDateLeft = dtLeft.toUTCString();
            if (c.clientId == socket.id) {
                c.timeLeft = utcDateLeft;
                c.active = false;
                clients[i] = c;
                console.log('Going to remove: ' + socket.id);
                break;
            }
        }
        count = count - 1;
        console.log(count);
        console.log('CONNECTION TERMINATED');
    });
});
http.listen(8091, () => {
    console.log('ExpressJS launched ::8091');
    app.post('/email_client_assignment', (req, res) => {
        res.end();
        console.log('ID' + req.body.id);
        console.log('Email' + req.body.email);
        for (var i = 0, len = clients.length; i < len; ++i) {
            var c = clients[i];
            if (c.clientId == req.body.id) {
                c.clientEmail = req.body.email;
                c.streamId = req.body.streamId;
                clients[i] = c;
                console.log('Email sucessfully assigned to client!');
                break;
            }
        }
    });
    app.get('/graphdata', (req, res) => {
        const streamId = req.query.streamId;
        console.log(streamId);
        StreamConnection.find({}, (err, data) => {
            if (err) {
                throw err;
            }
            else {
                let user_arr = Object.keys(data).map(function (key) {
                    return data[key];
                });
                let result = [];
                let count = 0;
                let timestamp = '';
                user_arr.forEach(element => {
                    if (element.streamId === streamId) {
                        count = count + 1;
                    }
                });
                user_arr.forEach(element => {
                    if (element.streamId === streamId) {
                        timestamp = element.timestamp;
                        const time = new Date(timestamp).getUTCHours().toLocaleString() + ":" + new Date(timestamp).getUTCMinutes().toLocaleString();
                        const tmp_element = { count, timestamp: time };
                        result.push(tmp_element);
                    }
                });
                res.send(result);
            }
        });
    });
    app.get('/log_data', (req, res) => {
        const streamId = req.query.streamId;
        console.log(streamId);
        StreamConnection.find({}, (err, data) => {
            if (err) {
                throw err;
            }
            else {
                let user_arr = Object.keys(data).map(function (key) {
                    return data[key];
                });
                let result = [];
                let timeOfJoin = '';
                user_arr.forEach(element => {
                    // determine timeOfJoin
                    if (element.streamId === streamId) {
                        if (timeOfJoin.length === 0) {
                            timeOfJoin = element.timestamp;
                        }
                        else {
                            const existingDate = new Date(timeOfJoin);
                            const newDate = new Date(element.timestamp);
                            if (newDate < existingDate) {
                                timeOfJoin = element.timestamp;
                            }
                        }
                    }
                });
                user_arr.forEach(element => {
                    if (element.streamId === streamId) {
                        if (element.active === false) {
                            const tmp_element = { email: element.clientEmail, timeOfJoin: timeOfJoin, timeLeft: element.timeLeft };
                            result.push(tmp_element);
                        }
                    }
                });
                res.send(result);
            }
        });
    });
});
cron.schedule('*/1 * * * *', () => {
    console.log('Connection report status task started!');
    clients.forEach((client, index) => {
        console.log(index);
        const dtTimestamp = new Date();
        const utcTimestamp = dtTimestamp.toUTCString();
        client.timestamp = utcTimestamp;
        const dbClient = new StreamConnection(client);
        if (dbClient.clientEmail.length > 0) {
            dbClient.save().then(() => console.log('Backup of client state with ID: ' + client.clientId + ' was sucessfull'));
            if (client.timeLeft.length > 0) {
                clients.splice(index, 1);
            }
        }
    });
    console.log('Connection report status task ended!');
});
